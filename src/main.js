import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

import VueFormulate from "@braid/vue-formulate";
import "../node_modules/@braid/vue-formulate/themes/snow/snow.scss";
Vue.use(VueFormulate);

import "@/assets/styles/style.sass";

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
