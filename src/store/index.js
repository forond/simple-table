import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import { table } from "./modules/table.module";

export default new Vuex.Store({
  modules: {
    table,
  },
});
