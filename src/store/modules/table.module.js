import axios from "axios";

const url = "http://www.filltext.com/";

export const table = {
  namespaced: true,
  state: {
    data: null,
  },
  mutations: {
    setData: (state, value) => {
      state.data = value;
    },
    addRow: (state, row) => {
      state.data.unshift(row);
    },
  },
  actions: {
    loadData({ commit }, props) {
      return axios
        .get(
          url +
            "?rows=" +
            props.size +
            "&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}"
        )
        .then((response) => {
          commit("setData", response.data);
        });
    },
    createRow({ commit }, props) {
      let row = {
        id: parseInt(props.item.id.value),
        firstName: props.item.firstName.value.toString(),
        lastName: props.item.lastName.value.toString(),
        email: props.item.email.value.toString(),
        phone: props.item.phone.value.toString(),
      };
      commit("addRow", row);
    },
  },
  getters: {
    getData: (state) => {
      return state.data;
    },
  },
};
